import java.util.ArrayList;
import java.util.List;

public abstract class Ora {

    private int maxHely;
    private int foglaltHely;

    private List<Hallgato> nevsor = new ArrayList<Hallgato>();  //listában tárolom azokat akik ezt az órát vették fel

    public Ora(int maxHely) {
        this.maxHely = maxHely;
    }

    public boolean jelentkezes(Hallgato hallgato){
        if(maxHely > foglaltHely){
            foglaltHely++;
            nevsor.add(hallgato);
            return true;
        } else
            return false;
    }

    public int getFoglaltHely() {
        return foglaltHely;
    }

    public void nevsor (){
        System.out.println("Ezt az órát felvette: " + nevsor);
    }
}
