public class OraFactory {

    public Ora makeOra(String newOra, int maxHely){

        if(newOra.equals("ProgOra")){
            return new ProgOra(maxHely);
        } else if(newOra.equals("LogikaOra")){
            return new LogikaOra(maxHely);
        }

        else return null;
    }
}
