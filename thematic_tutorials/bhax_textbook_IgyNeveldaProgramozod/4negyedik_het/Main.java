public class Main {
    public static void main(String[] args) {

        Integer n = 10;
        Integer[] Tomb = new Integer[n];

        add(3, Tomb);
        add(10, Tomb);
        add(2, Tomb);
        add(33, Tomb);
        add(45, Tomb);

        add(76, Tomb);
        add(4, Tomb);
        add(43, Tomb);
        add(55, Tomb);
        add(64, Tomb);


        System.out.println(Tomb[0]);
        System.out.println(Tomb[1]);
        System.out.println(Tomb[2]);
        System.out.println(Tomb[3]);
        System.out.println(Tomb[4]);

        System.out.println(Tomb[5]);
        System.out.println(Tomb[6]);
        System.out.println(Tomb[7]);
        System.out.println(Tomb[8]);
        System.out.println(Tomb[9]);

        System.out.println("-------");

        bubbleSort(Tomb);
        System.out.println(Tomb[0]);
        System.out.println(Tomb[1]);
        System.out.println(Tomb[2]);
        System.out.println(Tomb[3]);
        System.out.println(Tomb[4]);

        System.out.println(Tomb[5]);
        System.out.println(Tomb[6]);
        System.out.println(Tomb[7]);
        System.out.println(Tomb[8]);
        System.out.println(Tomb[9]);

    }

    static void add(Integer value, Integer[] Tomb){

        for(int i = 0; i < Tomb.length; i++){
            if(Tomb[i] == null){
                Tomb[i] = value;
                break;
            }
        }
    }

    static void bubbleSort(Integer[] Tomb){
        Integer tmp;

        for(int i = Tomb.length - 1; i > 0; i--){
            for(int j = 0; j < i; j++){
                if (Tomb[j] > Tomb[j+1]){
                    tmp = Tomb[j];
                    Tomb[j] = Tomb[j+1];
                    Tomb[j+1] = tmp;
                }
            }
        }
    }
}
