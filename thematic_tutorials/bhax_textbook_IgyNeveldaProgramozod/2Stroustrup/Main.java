public class Main {

    public static void main(String[] args) {
        int number = Integer.MAX_VALUE;
        System.out.println(Integer.MAX_VALUE);

        while (true) {
            try {
                System.out.println("Megpróbálom lefoglalni a következő értékkel: " + number);
                double[] d = new double[number];
                System.out.println("Foglalás sikeres!");
                break;
            } catch (OutOfMemoryError e) {
                System.out.println("------Túl nagy érték, megfelezem.");
                number /= 2;
            }
        }
    }
}