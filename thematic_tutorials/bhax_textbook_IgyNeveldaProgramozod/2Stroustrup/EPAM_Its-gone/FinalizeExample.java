package com.epam.training.finalize;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FinalizeExample {

    public static void main(String[] args) throws IOException {
        BugousStuffProducer stuffProducer = new BugousStuffProducer("someFile.txt");
        stuffProducer.writeStuff();
        //stuffProducer = null;
        System.gc();    // Csak hogy egy kicsit determinisztikusabbá tegyük a dolgot.
        //System.out.println(stuffProducer);
        /*
        Itt a stuffProducer még elérhető lehet (a JVM optimalizáció során rendelhet null értéket
        a változóhoz, mivel a továbbiakban nincs használva, de erre nincs garancia), ráadásul a main()
        befejezése sem garantálja, hogy a finalize() lefut erre az objektumra.

        */

    }

    private static class BugousStuffProducer {
        private final Writer writer;

        public BugousStuffProducer(String outputFileName) throws IOException {
            writer = new FileWriter(outputFileName);
        }

        public void writeStuff() throws IOException {
            writer.write("Stuff");
            System.out.println("write lefut");

        }


        @Override
        public void finalize() throws IOException {
            writer.close();
            System.out.println("close lefut");
        }

        @Override
        public String toString() {
            return "BugousStuffProducer{}   még itt vagyok";
        }
    }
}
