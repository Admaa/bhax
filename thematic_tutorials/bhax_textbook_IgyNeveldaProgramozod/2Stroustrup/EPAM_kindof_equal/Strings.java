public class Strings {
    public static void main(String[] args) {
        // Given
        String first = "...";
        String second = "...";
        String third = "..".concat(".");
        // When
        var firstMatchesSecondWithEquals = first.equals(second);        //legyen true
        var firstMatchesSecondWithEqualToOperator = first == second;                //true
        var firstMatchesThirdWithEquals = first.equals(third);              //true
        var firstMatchesThirdWithEqualToOperator = first == third;                  //false


        System.out.println(firstMatchesSecondWithEquals + " firstMatchesSecondWithEquals");
        System.out.println(firstMatchesSecondWithEqualToOperator + " firstMatchesSecondWithEqualToOperator");
        System.out.println(firstMatchesThirdWithEquals + " firstMatchesThirdWithEquals");
        System.out.println(firstMatchesThirdWithEqualToOperator + " firstMatchesThirdWithEqualToOperator");
    }

}
