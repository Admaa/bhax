package TurboRaceCar;


/**
* @generated
*/
public class TurboRaceCar extends RaceCar {
    
    /**
    * @generated
    */
    private String upgrade;
    
    
    
    /**
    * @generated
    */
    public String getUpgrade() {
        return this.upgrade;
    }
    
    /**
    * @generated
    */
    public String setUpgrade(String upgrade) {
        this.upgrade = upgrade;
    }
    
    
}
