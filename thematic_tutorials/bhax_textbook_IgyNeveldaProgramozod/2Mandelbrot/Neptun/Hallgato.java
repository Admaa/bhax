import java.util.ArrayList;
import java.util.List;

public class Hallgato {

    private String neptunID;
    private List<Ora> felvettOrak = new ArrayList<Ora>();

    public Hallgato(String neptunID) {
        this.neptunID = neptunID;
    }

    private boolean inList(Ora ora){
        for(int i = 0; i < felvettOrak.size(); i++){
            if(felvettOrak.get(i).getClass() == ora.getClass()) {
                return true;
                //System.out.println("benne van");
            }
        }
        return false;
    }


    public void felvesz(Ora ora){

        if (inList(ora)){
            System.out.println(this + " már felvette a következő órát: " + ora.getClass());
        }
                    /*if (felvettOrak.contains(ora)) { //ez csak adott órát vizsgált, nem magát a típusát
                        System.out.println(this + " Ez az óra már fel van véve!!");
                    }*/
            else if(ora.jelentkezes(this) == true){

            felvettOrak.add(ora);
            System.out.println(this + " Sikeres tárgyfelvétel!");

        }
            else {
            System.out.println(this + " Hiba, nincs elég férőhely");
        }
    }

    public void hallgatoOrai (){
        System.out.println(felvettOrak);
    }

    @Override
    public String toString() {
        return neptunID;
    }
}
