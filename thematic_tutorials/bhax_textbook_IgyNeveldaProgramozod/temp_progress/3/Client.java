public class Client {
    public static void main(String[] args) {

        Hallgato hallgato1 = new Hallgato("ASD123");
        Hallgato hallgato2 = new Hallgato("abc231");
        Hallgato hallgato3 = new Hallgato("sss111");
        Hallgato hallgato4 = new Hallgato("fff222");
        Hallgato hallgato5 = new Hallgato("00011f");

        OraFactory oraFactory = new OraFactory();       //Az órák létrehozásához alkalmazom a Factory Method-ot


        Ora progOra1 = oraFactory.makeOra("ProgOra", 3);
        Ora progOra2 = oraFactory.makeOra("ProgOra", 2);
        Ora logikaOra1 = oraFactory.makeOra("LogikaOra", 4);
        Ora logikaOra2 = oraFactory.makeOra("LogikaOra", 2);


        hallgato1.felvesz(progOra1);
        hallgato2.felvesz(progOra1);
        hallgato3.felvesz(progOra1);
        hallgato4.felvesz(progOra1);
        hallgato5.felvesz(progOra1);
        System.out.println("---\n");

        System.out.println(progOra1.getFoglaltHely());
        hallgato1.hallgatoOrai();
        progOra1.nevsor();

        System.out.println("-----\n");
        hallgato1.felvesz(logikaOra2);
        hallgato2.felvesz(logikaOra2);
        hallgato3.felvesz(logikaOra2);

        System.out.println(logikaOra2.getFoglaltHely());
        logikaOra2.nevsor();

        hallgato1.hallgatoOrai();

        hallgato1.felvesz(progOra2);
    }
}
