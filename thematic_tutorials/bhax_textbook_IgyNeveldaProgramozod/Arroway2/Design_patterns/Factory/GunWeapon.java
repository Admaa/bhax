package factory;

public class GunWeapon extends Weapon{

    public GunWeapon() {
        setName("Gun");
        setPrice(500);
    }

    @Override
    public String toString() {
        return getName() +
                " for " +
               getPrice() +
                "$";
    }
}
