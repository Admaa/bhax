package factory;

public abstract class Weapon {

    private String name;
    private int price;

    public String getName(){
        return name;
    }
    public void setName(String newName){
        name = newName;
    }

    public int getPrice(){
        return price;
    }
    public void setPrice(int newPrice){
        price = newPrice;
    }
}
