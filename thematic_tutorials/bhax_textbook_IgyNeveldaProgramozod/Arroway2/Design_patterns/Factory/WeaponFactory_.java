package factory;

public class WeaponFactory {

    public Weapon makeWeapon(String newOrder) {

        if(newOrder.equals("Knife")){
            return new KnifeWeapon();
        } else if(newOrder.equals("Gun")){
            return new GunWeapon();
        } else return null;
    }
}
