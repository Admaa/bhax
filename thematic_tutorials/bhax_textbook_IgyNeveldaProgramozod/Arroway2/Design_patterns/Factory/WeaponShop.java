package factory;

public class WeaponShop {

    public static void main(String[] args) {

        WeaponFactory weaponFactory = new WeaponFactory();

        Weapon customer1, customer2;

        String order = "Knife";
        //String order = "Gun";

        customer1 = weaponFactory.makeWeapon(order);
        System.out.println("You bought a " + customer1);
        customer2 = weaponFactory.makeWeapon("Gun");


        if(customer1.getName() == "Knife" && customer2.getName() == "Gun"){

            System.out.println("Huh, brings a knife to a gunfight.");

        } else if(customer1.getName() == "Gun"){

            System.out.println("Well well well. How the turntables.");
        }
    }
}
