public abstract class Vehicle {

    private String name;


    Engine engine;
    Wheel wheel;

    public String getName() { return name; }
    public void setName(String newName) { name = newName; }

    abstract void buildVehicle();


    public String toString(){

        return "This " + name + " has a " + engine + " and a " + wheel;
    }
}
