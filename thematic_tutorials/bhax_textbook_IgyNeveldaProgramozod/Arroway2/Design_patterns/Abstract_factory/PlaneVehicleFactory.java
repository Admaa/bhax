public class PlaneVehicleFactory implements VehicleFactory{

    public Engine addEngine(){
        return new PlaneEngine();
    }

    public Wheel addWheel(){
        return new PlaneWheel();
    }
}