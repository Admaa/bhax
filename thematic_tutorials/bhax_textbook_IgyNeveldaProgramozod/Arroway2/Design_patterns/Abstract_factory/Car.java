public class Car extends Vehicle{

    VehicleFactory vehicleFactory;

    public Car(VehicleFactory vehicleFactory){
        this.vehicleFactory = vehicleFactory;
    }

    void buildVehicle(){
        System.out.println("Building a " + getName());

        engine = vehicleFactory.addEngine();
        wheel = vehicleFactory.addWheel();
    }
}