public class Plane extends Vehicle{

    VehicleFactory vehicleFactory;

    public Plane(VehicleFactory vehicleFactory){
        this.vehicleFactory = vehicleFactory;
    }

    void buildVehicle(){
        System.out.println("Building a " + getName());

        engine = vehicleFactory.addEngine();
        wheel = vehicleFactory.addWheel();
    }
}