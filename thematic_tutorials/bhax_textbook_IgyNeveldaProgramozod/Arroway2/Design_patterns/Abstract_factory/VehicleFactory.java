public interface VehicleFactory {

    public Engine addEngine();
    public Wheel addWheel();
}
