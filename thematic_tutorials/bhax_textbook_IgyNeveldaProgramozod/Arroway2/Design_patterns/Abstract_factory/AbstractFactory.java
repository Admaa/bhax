public abstract class AbstractFactory {

    protected abstract Vehicle makeVehicle(String typeOfVehicle);


    public Vehicle orderVehicle(String typeOfVehicle){
        Vehicle theVehicle = makeVehicle(typeOfVehicle);

        theVehicle.buildVehicle();


        return theVehicle;
    }
}
