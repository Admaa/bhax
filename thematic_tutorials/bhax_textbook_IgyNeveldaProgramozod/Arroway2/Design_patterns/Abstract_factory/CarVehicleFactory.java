public class CarVehicleFactory implements VehicleFactory{

    public Engine addEngine(){
        return new CarEngine();
    }

    public Wheel addWheel(){
        return new CarWheel();
    }
}