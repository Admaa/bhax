public class Client {
    public static void main(String[] args) {

        GyrosVendor gyrosVendor1 = GyrosVendor.getInstance(1000);
        GyrosVendor gyrosVendor2 = GyrosVendor.getInstance(1200);

        System.out.println(gyrosVendor1.price);
        System.out.println(gyrosVendor2.price);

        gyrosVendor1.price = 900;

        System.out.println(gyrosVendor1.price);
        System.out.println(gyrosVendor2.price);

    }
}
