public class GyrosVendor {

    public int price;

    private static GyrosVendor instance;

    private GyrosVendor(int price) {
        this.price = price;
    }

    public static GyrosVendor getInstance(int price) {

        if (instance == null) {
            instance = new GyrosVendor(price);
        }
        return instance;
    }
}
