public class PaintedShirt extends Shirt{

    public String Design;   //Drawing, Text

    public PaintedShirt() {
    }

    public PaintedShirt(PaintedShirt original) {
        super(original);
        if (original != null) {
            this.Design = original.Design;
        }
    }

    @Override
    public Shirt clone() {
        return new PaintedShirt(this);
    }

    @Override
    public String toString() {
        return "PaintedShirt{" +
                "Design='" + Design + '\'' +
                ", Color='" + Color + '\'' +
                ", Material='" + Material + '\'' +
                ", Style='" + Style + '\'' +
                '}';
    }
}

