public abstract class Shirt {

    public String Color;
    public String Material;
    public String Style;

    public Shirt() {
    }

    public Shirt(Shirt original){
        if(original != null){
            this.Color  = original.Color;
            this.Material = original.Material;
            this.Style = original.Style;

        }
    }

    public abstract Shirt clone();

    @Override
    public String toString() {
        return "Shirt{" +
                "Color='" + Color + '\'' +
                ", Material='" + Material + '\'' +
                ", Style='" + Style + '\'' +
                '}';
    }
}
