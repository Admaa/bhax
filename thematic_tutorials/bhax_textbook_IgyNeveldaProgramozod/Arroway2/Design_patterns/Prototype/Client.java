public class Client {
    public static void main(String[] args) {

        BasicShirt basicShirt = new BasicShirt();
        basicShirt.Color = "Black";
        basicShirt.Material = "Cotton";
        basicShirt.Style = "Plain";

        BasicShirt basicshirtClone = (BasicShirt) basicShirt.clone();

        System.out.println("basicShirt: " + basicShirt);
        System.out.println("basicshirtClone: " + basicshirtClone);




        PaintedShirt paintedShirt1 = new PaintedShirt();

        paintedShirt1.Color = "Black";
        paintedShirt1.Material = "Cotton";
        paintedShirt1.Style = "Plain";
        paintedShirt1.Design = "Drawing";

        PaintedShirt  paintedShirt2 = (PaintedShirt) paintedShirt1.clone();


        System.out.println("paintedShirt1: " + paintedShirt1);
        System.out.println("paintedShirt2: " + paintedShirt2);

    }
}
