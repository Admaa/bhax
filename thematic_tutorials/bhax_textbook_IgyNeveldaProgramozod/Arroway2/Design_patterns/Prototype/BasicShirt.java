public class BasicShirt extends Shirt{

    public BasicShirt() {
    }

    public BasicShirt(BasicShirt original) {
        super(original);
    }

    @Override
    public Shirt clone() {
        return new BasicShirt(this);
    }
}
