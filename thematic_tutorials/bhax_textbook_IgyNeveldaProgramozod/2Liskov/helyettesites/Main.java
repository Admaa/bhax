public class Main {
    public static void main(String[] args) {

        Bird eagle = new Eagle();
        eagle.fly();

        System.out.println("-----");

        Bird penguin = new Penguin();
        penguin.fly();

    }
}
