#include <iostream>

using namespace std;

class Bird {
  public:
      void fly(){
        cout << "Preparing to fly..." << endl;
        clapWings();
      }
      void clapWings(){
        cout << "Clapping my wings..." << endl;
      }
};

class Eagle: public Bird{
public:
    Eagle() {
        cout << "Im an eagle." << endl;
    }

};

class Penguin: public Bird{
public:
    Penguin() {
        cout << "Im a penguin." << endl;
    }
    void fly(){
        cout << "I can not fly.";
    }

};

int main() {
  Eagle eagle;
  eagle.fly();

  cout << endl;

  Penguin penguin;
  penguin.fly();

  return 0;
}
