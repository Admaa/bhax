public class Penguin extends Bird{
    public Penguin() {
        System.out.println("Im a penguin.");
    }

    void fly(){
        System.out.println("I can not fly.");
    }
}
