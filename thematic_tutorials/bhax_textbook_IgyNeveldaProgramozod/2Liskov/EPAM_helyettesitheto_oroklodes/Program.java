public class Program {
    public static void main(String[] args) {

        Vehicle firstVehicle = new SuperCar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);

        System.out.println("----------");

        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.start();

        System.out.println(secondVehicle instanceof SuperCar);
            System.out.println(secondVehicle instanceof Car);
            System.out.println(secondVehicle instanceof Vehicle);
	    System.out.println(secondVehicle.getClass());

        //SuperCar thirdVehicle = new Vehicle();
        //thirdVehicle.start();


    }
}
