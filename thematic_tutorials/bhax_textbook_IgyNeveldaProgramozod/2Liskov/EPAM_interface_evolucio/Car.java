public interface Car {
    void carStart();

    default void carStop(){
        System.out.println("Car stop!");
        staticMethod();
    }

    static void staticMethod(){
        System.out.println("Ez egy staticMethod");
    }

    default void originalMethod(){
        System.out.println("Engem felülírnak");
    }
}
