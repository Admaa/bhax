public class MainJav implements Car{
    public static void main(String[] args) {
        MainJav m = new MainJav();
        m.carStart();
        m.carStop();
        m.originalMethod();
        Car.staticMethod();
        //m.staticMethod();
    }

    @Override
    public void carStart() {
        System.out.println("Car start!");
    }

    public void originalMethod(){
        System.out.println("Ez a felülírt szöveg");
    }
}
