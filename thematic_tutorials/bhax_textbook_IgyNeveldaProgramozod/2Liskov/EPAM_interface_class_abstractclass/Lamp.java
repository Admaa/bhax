public abstract class Lamp {

    public void turnOn(){
        System.out.println("The lamp turns on!");
    }

    public void turnOff(){
        System.out.println("The lamp turns off!");
    }

    public abstract void changeColor();
}
