public class StrangeLamp extends Lamp{
    @Override
    public void changeColor() {
        System.out.println("Changed light color to red!");
    }

    public void turnOff(){
        System.out.println("Why can't I turn it off?!");
    }
}
