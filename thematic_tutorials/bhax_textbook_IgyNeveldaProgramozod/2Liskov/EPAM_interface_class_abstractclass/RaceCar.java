public class RaceCar implements Car{

    @Override
    public void carGo() {
        System.out.println("Car goes!");
    }

    @Override
    public void carStop() {
        System.out.println("Car stops!");
    }
}
