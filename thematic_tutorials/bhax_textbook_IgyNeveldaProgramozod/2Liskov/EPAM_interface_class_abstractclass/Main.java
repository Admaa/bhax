public class Main {
    public static void main(String[] args) {

        SimpleClass sc = new SimpleClass();
        sc.doStuff();

        System.out.println("-----");

        Lamp sl = new StrangeLamp();
        sl.turnOn();
        sl.changeColor();
        sl.turnOff();

        System.out.println("-----");

        RaceCar rc = new RaceCar();
        rc.carGo();
        rc.carStop();
    }
}
