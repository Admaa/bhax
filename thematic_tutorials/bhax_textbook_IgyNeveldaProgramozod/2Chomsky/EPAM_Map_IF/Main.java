public class Main {
    public static void main(String[] args) {
        ArrayMap map = new ArrayMap();

        System.out.println("üres-e: " + map.isEmpty());

        map.put(2,5);
        System.out.println(map.get(2));

        map.put("text", 3);
        System.out.println(map.get("text"));

        map.put(true, false);
        System.out.println(map.get(true));

        map.put(map, map);
        System.out.println(map.get(map));

        System.out.println("Tartalmaz-e elemet aminek kulcsa 2: " + map.containsKey(2));


        System.out.println(map.entrySet());

        System.out.println("\nA value amit felülír: " + map.put(2,6));
        System.out.println("Az új érték: " + map.get(2));

        map.remove(map);
        System.out.println("Az az elem aminek a map objektum a kulcsa: " + map.get(map));



        ArrayMap map2 = new ArrayMap();
        map2.put("bonus", "map2_value");
        map2.put("bonus2", "map2_value2");
        map.putAll(map2);
        System.out.println(map.entrySet());

        System.out.println("A tárolt kulcsok: " + map.keySet());
        System.out.println("A tárolt értékek: " + map.values());

        map.clear();


    }
}
