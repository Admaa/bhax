import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Fullscreen {
    public static void main(String[] args) {
        GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = graphics.getScreenDevices(); //A devices tömbbe lekérjük az elérhető eszközöket

        GraphicsDevice device;
        if(devices.length > 1){
            device = devices[1]; //ha van több monitor akkor használjuk a másodikat..
        } else {
            device = devices[0];    //..különben az alapértelmezettet
        }
        System.out.println("Ennyi eszköz érhető el: " + devices.length);
        System.out.println("Erre a monitorra nyitjuk meg: " + device);

        JFrame frame = new JFrame("Fullscreen");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Az ablak bezárásával álljon le aprogram

        JLabel label = new JLabel("Kattintson ide a kilépéshez!", JLabel.CENTER); //Létrehozok egy címkét,
        label.setForeground(Color.gray);
        label.setFont(new Font("Verdana", Font.PLAIN, 30)); //formázom


        label.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                frame.dispose(); //A címkére kattintva kiléphetünk az ablakból
            }
        });

        frame.add(label);   //majd hozzáadom ezt a címkét a készülő ablakhoz

        frame.setUndecorated(true);
        frame.setBackground(new Color(242,238,203));    //háttérszín beállítása

        device.setFullScreenWindow(frame); //Ez a sor nyitja meg magát az ablakot
    }
}
