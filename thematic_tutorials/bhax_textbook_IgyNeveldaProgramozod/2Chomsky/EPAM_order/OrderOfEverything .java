import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderOfEverything {

    public static <T> List<T> createOrderedList(Collection<T> input) {
        return input.stream()
                .sorted()
                .collect(Collectors.toList());
    }


    public static void main(String[] args) {

        Collection<Integer> collI = new LinkedList<Integer>();

        collI.add(5);
        collI.add(3);
        collI.add(10);

        //System.out.println(collI.get(1)); //ez hibához vezet, mivel a collection csak egy kupac elem amik egy csoportba tartoznak, listával vagy tömbbel ellentétben nincsenek adott sorrendbe rakva.
        //System.out.println(collI[1]);

        System.out.println(collI);
        System.out.println("-----");


        List<Integer> outputI = createOrderedList(collI);

        System.out.println(outputI);
        System.out.println("-------\n");

        Collection<String> collS = new LinkedList<String>();

        collS.add("Hello");
        collS.add(", ");
        collS.add("World");
        collS.add("!");

        List<String> outputS = createOrderedList(collS);
        System.out.println(outputS);

        System.out.println("-------------\n");

        Obj obj1 = new Obj(2);
        Obj obj2 = new Obj(5);
        Obj obj3 = new Obj(1);


        Collection<Obj> collO = new LinkedList<Obj>();

        collO.add(obj1);
        collO.add(obj2);
        collO.add(obj3);
        System.out.println(collO);

        List<Obj> outputO = createOrderedList(collO);



    }


}


