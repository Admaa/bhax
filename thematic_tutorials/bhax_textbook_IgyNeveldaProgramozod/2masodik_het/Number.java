public class Number {

    static int szam(){      //ennek a metódusnak a komplexitása 5 (= 12 - 9 + 2)
        int n = -5;
        if (n > -10 && n < 10 && n != 0) {
            for (int i = 0; i < 3; i++) {
                n += i;
            }
            return n;
        }
        return 3;
    }
}
