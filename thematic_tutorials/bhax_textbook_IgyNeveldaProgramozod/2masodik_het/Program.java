public class Program {
    public static void main(String[] args) {

        Vehicle firstVehicle = new SuperCar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);        //mivel a Car osztály egy gyermeke, beltartozik a Car típusba is

        System.out.println("----------");

        Car secondVehicle = (Car) firstVehicle;     //hiába castoltuk SuperCar typusú objektumot Car típusúra..
        secondVehicle.start();                      //..az még mindig az eredeti, SuperCar osztályban felüldefiniált start()-ot használja

        System.out.println(secondVehicle instanceof SuperCar);  //..látható, hogy még a szülőosztály típusára való castolás utánn is rendelkezik a SuperCar típussal
            System.out.println(secondVehicle instanceof Car);
            System.out.println(secondVehicle instanceof Vehicle);

        //SuperCar thirdVehicle = new Vehicle();      //ez hibát eredményez, mivel a poliformizmus csak a másik irányba működik (szülő típús kaphat gyermek típusú egyedet, de fordítva nem. Itt most SuperCar leszármazottja Vehicle-nek, ezért nem alkalmazható a poliformizmus)
        //thirdVehicle.start();                         //poliformizmus (többalakúság): egy változó nem csak szigorúan a deklarált típusú, hanem leszármazott objektumokra is hivatkozhat.


    }
}
