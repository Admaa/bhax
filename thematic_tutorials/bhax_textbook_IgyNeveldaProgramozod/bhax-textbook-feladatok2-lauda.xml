<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>8. hét - „Helló, Lauda!”</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>       
         
        
    <section>
        <title>EPAM: DI</title>
            <para>
                Implementálj egy alap DI (Dependency Injection) keretrendszert Java-ban annotációk és reflexió
használatával megvalósítva az IoC-t (Inversion Of Control).
            </para>
            
            <para>
                ...készülőben...
            </para>
            <para>
                Reflection segítségével nyerehtünk ki adatot olyan objektumól aminek nem tudjuk a típusát.
            </para>
            <para>
                Dependency injecionn: ha már létrehoztunk egy példányt egy service-ből, akkor ha nem szükséges később ne gyártsunk belőle többet. Helyette a már létezőt adjuk át paraméternek. Erre példa a JSON-serialization feladatból a mainSerializer. Ezt már egyszer létrehoztuk a serializerList és nullserializer paraméterekkel. Később csupán ezt az egyedet adjuk oda ha egy MainSerializer-re van szükség.
            </para>

    </section>

    <section>
        <title>EPAM: JSON szerializáció</title>
            <para>
                Implementálj egy JSON szerializációs könyvtárat, mely képes kezelni sztringeket, számokat, listákat
és beágyazott objektumokat. A megoldás meg kell feleljen az összes adott unit tesztnek.
Plusz feladat:
            </para>
            <para>
            Források: 
            <link xlink:href="https://github.com/epam-deik-cooperation/epam-deik-prog2">https://github.com/epam-deik-cooperation/epam-deik-prog2</link>
        </para>

        <para>
            A Serialize interfész alapértelmezésben generikus típusú. Ennek implementáláskor kell megadni típusát, azaz, hogy milyen paramétereket várunk. Két metódust deklarál: A getSourceClass()-t ami visszaadja azt a forrásosztályt amire hívható.
Emellett egy String típusú serialize-ot aminek paraméterei egy az interfész típusával megegyező típusú objektum, és egy újabb példánya a Serializer-nek, hogy később lehessen rekurzívan hívni.
        </para>



        <para>
            BooleanSerializer esetében  az implementált serializer típusa Boolean.
A String serialize() Boolean típusú objektumból állít elő valamilyen szambály szerint egy stringet.
Boolean, Null, Number, String szerializálásának szintaxisa nagyjából egyezik, így ezt nem részletezem külön.
                <programlisting language="java">
<![CDATA[package com.epam.training.serializer;

/**
 * Serializer implementation for {@link Boolean} instances.
 */
public class BooleanSerializer implements Serializer<Boolean> {

    @Override
    public String serialize(Boolean obj, Serializer<Object> mainSerializer) {
        return obj ? "true" : "false";
    }

    @Override
    public Class<Boolean> getSourceClass() {
        return Boolean.class;
    }

}]]>
             </programlisting>
        </para>



        <para>
            ListSerializer egy Lista típusú objektummal dolgozik. Ez egy streamet indít, melyben rekurzív módon szerializálja a lista egyes elemeit, majd ezeket összefűzi a .joining() metódussal és String-ként visszaadja.
                <programlisting language="java">
<![CDATA[package com.epam.training.serializer;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Serializer implementation for {@link List} instances.
 */
public class ListSerializer implements Serializer<List> {

    @Override
    public String serialize(List obj, Serializer<Object> mainSerializer) {
        return (String) obj.stream()
                .map(o -> mainSerializer.serialize(o, mainSerializer))
                .collect(Collectors.joining(",", "[", "]"));
    }

    @Override
    public Class<List> getSourceClass() {
        return List.class;
    }

}
]]>
             </programlisting>
        </para>



        <para>
            Object szerializációnál egy streamben először kiszűrjük az object fieldjeit. A fieldeknek egy try-catch blokkban próbáljuk kinyerni a tartalmát. Ha ez hibát ad akkor valószínű, hogy adott elemhez nem volt hozzáférési jog. Előfordulhat, hogy egy osztályon belül egy másik osztály van definiálva. Ilyen többrétegű object-eket rekurzívan szerializáljuk, azaz úgyanúgy meghívódik rá az ObjectSerializer. Mivel map-et használunk így értékpárokat kapunk. Ezeket a String értékpárokat a .format()-al formázzuk a minta szerint, majd csatoljuk a többihez.

                <programlisting language="java">
<![CDATA[public String serialize(Object obj, Serializer<Object> mainSerializer) {
        return Arrays.stream(obj.getClass().getDeclaredFields())
                .map(field -> {
                    try {
                        String name = field.getName();

                        if (!field.canAccess(obj)) {
                            field.setAccessible(true);
                        }

                        String value = mainSerializer.serialize(field.get(obj), mainSerializer);

                        return String.format("\"%s\":%s", name, value);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    return "";
                })
                .collect(Collectors.joining(",", "{", "}"));
    }]]>
             </programlisting>
        </para>



        <para>
            A MiniObjectMapper osztályban létrehozunk egy listát a serializer osztályoknak.
Konstruktorában egy MainSerializer-t hoz létre, ami megkapja a különböző serializer osztályok listáját.
Amikor meghívódik a toJson() akkor igazából a MainSerializer serialize metódusa kapja meg paraméterül az átalakítandó objektumot és önmagát(MainSerializer).
                <programlisting language="java">
<![CDATA[.
.
public class MiniObjectMapper {

  private static final Serializer<?> nullSerializer = new NullSerializer();
  private static final List<Serializer<?>> SERIALIZER_LIST = List.of(
          new StringSerializer(),
          new NumberSerializer(),
          new BooleanSerializer(),
          new ListSerializer(),
          new ObjectSerializer()
  );

  private final MainSerializer mainSerializer;
.
.
public MiniObjectMapper() {
    this.mainSerializer = new MainSerializer(SERIALIZER_LIST, nullSerializer);
  }
.
.
public String toJson(Object obj) {
    return mainSerializer.serialize(obj, mainSerializer);
  }]]>
             </programlisting>
        </para>



        <para>
            A MainSerializer osztályban a selectSerializer-el választjuk ki melyik típusú serializert használjuk. Egy streamben végigmegyünk a serializerList-en és keresünk egy olyan típusú serializert ami passzol a kapott object-hez. Ha nincs ilyen típus akkor már csak NullSerializer lehet.
                <programlisting language="java">
<![CDATA[private Serializer<?> selectSerializer(Object obj) {
        return serializerList.stream()
                .filter(serializer -> serializer.getSourceClass().isInstance(obj))
                .findFirst()
                .orElse(nullSerializer);
    }]]>
             </programlisting>
        </para>



        <para>
            Ha megvan a megfelelő típusú serializerünk akkor már hívhatjuk is annak serialize metófusát.
                <programlisting language="java">
<![CDATA[@Override
    public String serialize(Object obj, Serializer<Object> mainSerializer) {
        Serializer<Object> selectedSerializer = (Serializer<Object>) selectSerializer(obj);
        return selectedSerializer.serialize(obj, this);
    }]]>
             </programlisting>
        </para>



        <para>
            Teszt futatása után a következő JSON fájlt kapjuk:
                <programlisting language="java">
<![CDATA[{"string":"outerStringSerialized","primitiveInt":1113,"boxedInt":1134,"boxedLong":1515,"boxedDouble":3.141592654,"stringList":["first","second","third"],"innerData":{"innerString":"innerStringSerialized","innerPrimitiveInt":2131,"nullObject":null},"aBoolean":false}]]>
             </programlisting>
        </para>
    </section>

    <section>
        <title>EPAM: Kivételkezelés</title>
            <para>
                Adott az alábbi kódrészlet. Mi történik, ha az input változó 1F, “string” vagy pedig null? Meghívódik
e minden esetben a finally ág? Válaszod indokold!
            </para>
            <para>
            Források: 
            <link xlink:href="https://dev.to/the_unconventional_coder/java-7-java-8-pea">https://dev.to/the_unconventional_coder/java-7-java-8-pea</link>
        </para>

        <para>
            Megoldás forrása:
            <link xlink:href="2Lauda/ExceptionHandling.java">
                <filename>2Lauda/ExceptionHandling.java</filename>
            </link>
        </para>

        <para>
            A kivételkezelés hatalmas segítséget jelenthet olyan kódrészeknél, melyeknél tudjok, hogy nem garantált a hiba mentes működés. Ilyenek például gép és felhasználó közi I/O műveletek, vagy egy hálózaton való kommunikáció.
Ahelyett, hogy a hiba miatt az egész program leállna, a veszélyes kódotrészt egy try-catch blokkba helyezve elkaphatjuk ha valamilyen hibát dob. Adott kódhoz érdemes saját hibakódokat definiálni, mivel ezeket sokkal hatékonyabban tudjuk lekezelni.
        </para>

        <para>
            input = null esetén RuntimeException()-t dob a kód. Mivel ehez nem adtunk külön catch blokkot így ezt a catch(Exception e) generikus kivétel kapja el. Végül lefut a finally blokk.
        </para>

        <para>
            input = 1F esetén ChildException()-dobódik amit le is kezelünk. Lefut a finally, de még elefut a tesztet hívó try csatch ága is, mivel lekezeléskor dobtunk egy újabb kivételt, egy ParentException()-t amit nem kezeltünk le.
        </para>

        <para>
            input = "string" esetén ParentException()-t dob és kap el. A lekezelő részben System.exit(1); parancsal megszakítja a programot, emiatt ennél nem fut le a finally blokk.
        </para>

        <para>
            Mivel ChildException() a ParentException() leszármazottja, így ha ChildException() nincs lekezelve, de egy szülője igen akkor ő is a szülő lekezelését használja majd. Ennek szemléltetésére töröljük ki a catch (ChildException e) blokkot. input = float továbbra is ChildException()-t dob, de ezt már a catch (ParentException e) kezeli.
        </para>
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
