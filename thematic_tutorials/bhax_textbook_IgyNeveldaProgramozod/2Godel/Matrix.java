import java.util.Arrays;
import java.util.stream.IntStream;

public class Matrix {
    public static void main(String[] args) {
        double[][] m1 = { { 4, 8 }, { 0, 2 }, { 1, 6 } };
        double[][] m2 = { { 5, 2 }, { 9, 4 } };

        double[][] result = Arrays.stream(m1).map(r ->
                IntStream.range(0, m2[0].length).mapToDouble(i ->
                        IntStream.range(0, m2.length).mapToDouble(j -> r[j] * m2[j][i]).sum()
                ).toArray()).toArray(double[][]::new);

        System.out.println(Arrays.deepToString(result));


        //m1 x m2
        double[][] resultFor= new double[m1.length][m2[0].length];
        double sum = 0;

        for(int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m2[0].length; j++) {
                for (int k = 0; k < m1[0].length; k++){
                    sum = sum + m1[i][k] * m2[k][j];
                }
                resultFor[i][j] = sum;
                sum = 0;
            }
        }

        System.out.println(Arrays.deepToString(resultFor));
    }
}
