from __future__ import print_function
# ------------------------------------------------------------------------------------------------
# Copyright (c) 2016 Microsoft Corporation
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ------------------------------------------------------------------------------------------------

# Tutorial sample #2: Run simple mission using raw XML

# Added modifications by Norbert Bátfai (nb4tf4i) batfai.norbert@inf.unideb.hu, mine.ly/nb4tf4i.1
# 2018.10.18, https://bhaxor.blog.hu/2018/10/18/malmo_minecraft
# 2020.02.02, NB4tf4i's Red Flowers, http://smartcity.inf.unideb.hu/~norbi/NB4tf4iRedFlowerHell


from builtins import range
import MalmoPython
import os
import sys
import time
import random
import json
import math
from enum import Enum

if sys.version_info[0] == 2:
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately
else:
    import functools
    print = functools.partial(print, flush=True)

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse( sys.argv )
except RuntimeError as e:
    print('ERROR:',e)
    print(agent_host.getUsage())
    exit(1)
if agent_host.receivedArgument("help"):
    print(agent_host.getUsage())
    exit(0)

# -- set up the mission -- #
missionXML_file='nb4tf4i_d_5x5x5.xml'
with open(missionXML_file, 'r') as f:
    print("NB4tf4i's Red Flowers (Red Flower Hell) - DEAC-Hackers Battle Royale Arena\n")
    print("NB4tf4i vörös pipacsai (Vörös Pipacs Pokol) - DEAC-Hackers Battle Royale Arena\n\n")
    print("The aim of this first challenge, called nb4tf4i's red flowers, is to collect as many red flowers as possible before the lava flows down the hillside.\n")
    print("Ennek az első, az nb4tf4i vörös virágai nevű kihívásnak a célja összegyűjteni annyi piros virágot, amennyit csak lehet, mielőtt a láva lefolyik a hegyoldalon.\n")    
    print("Norbert Bátfai, batfai.norbert@inf.unideb.hu, https://arato.inf.unideb.hu/batfai.norbert/\n\n")    
    print("Loading mission from %s" % missionXML_file)
    mission_xml = f.read()
    my_mission = MalmoPython.MissionSpec(mission_xml, True)
    my_mission.drawBlock( 0, 0, 0, "lava")


class Hourglass:
    def __init__(self, charSet):
        self.charSet = charSet
        self.index = 0
    def cursor(self):
        self.index=(self.index+1)%len(self.charSet)
        return self.charSet[self.index]

hg = Hourglass('|/-\|')

class SteveState(Enum): 
	PREPARE = 0;
	SPIRAL = 1;
	RIGH_FLOWER = 2;
	LEFT_FLOWER = 3;

    #TESZT = 20;

class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host
        self.x = 0
        self.y = 0
        self.z = 0        
        self.yaw = 0
        self.pitch = 0

        self.turns = 0


        self.state = SteveState.PREPARE
        #self.state = SteveState.MUST_GO_UP
        #self.state = SteveState.TESZT



        self.front_of_me_idx = 0
        self.front_of_me_idxr = 0
        self.front_of_me_idxl = 0        
        self.right_of_me_idx = 0
        self.left_of_me_idx = 0
        self.kettovel_elottem_idx = 0
        self.kettoval_balra = 0

    def calcNbrIndex(self):
        if self.yaw >= 180-22.5 and self.yaw <= 180+22.5 :
            self.front_of_me_idx = 32
            self.front_of_me_idxr = 33
            self.front_of_me_idxl = 31
            self.right_of_me_idx = 38
            self.left_of_me_idx = 36
            self.kettovel_elottem_idx = 27
            self.kettoval_balra = 35                       
        elif self.yaw >= 270-22.5 and self.yaw <= 270+22.5 :
            self.front_of_me_idx = 38
            self.front_of_me_idxr = 43
            self.front_of_me_idxl = 33
            self.right_of_me_idx = 42
            self.left_of_me_idx = 32
            self.kettovel_elottem_idx = 39
            self.kettoval_balra = 27                                               
        elif self.yaw >= 360-22.5 or self.yaw <= 0+22.5 :
            self.front_of_me_idx = 42
            self.front_of_me_idxr = 41
            self.front_of_me_idxl = 43
            self.right_of_me_idx = 36
            self.left_of_me_idx = 38
            self.kettovel_elottem_idx = 47
            self.kettoval_balra = 39                                              
        elif self.yaw >= 90-22.5 and self.yaw <= 90+22.5 :
            self.front_of_me_idx = 36
            self.front_of_me_idxr = 31
            self.front_of_me_idxl = 41
            self.right_of_me_idx = 32
            self.left_of_me_idx = 42
            self.kettovel_elottem_idx = 35
            self.kettoval_balra = 47                        
                       
        else:
            print("There is great disturbance in the Force...")

    def idle(self, delay):
        #print("      SLEEPING for ", delay)
        time.sleep(delay)

    def whatMyPos(self, observations):
        if "Yaw" in observations:
            self.yaw = int(observations["Yaw"])
        if "Pitch" in observations:
            self.pitch = int(observations["Pitch"])
        if "XPos" in observations:
            self.x = int(observations["XPos"])
        if "ZPos" in observations:
            self.z = int(observations["ZPos"])        
        if "YPos" in observations:
            self.y = int(observations["YPos"])


        
    def run(self):
        world_state = self.agent_host.getWorldState()
        # Loop until mission ends:
        while world_state.is_mission_running:
            
            #print(">>> nb4tf4i arena -----------------------------------\n")
            delay = self.action(world_state)
            #print("nb4tf4i arena >>> -----------------------------------\n")
            self.idle(delay)
                                
            world_state = self.agent_host.getWorldState()

    def action(self, world_state):
        for error in world_state.errors:
            print("Error:", error.text)
        
        if world_state.number_of_observations_since_last_state == 0:
            #print("    NO OBSERVATIONS NO ACTIONS")
            return False
        
        input = world_state.observations[-1].text
        observations = json.loads(input)
        nbr = observations.get("nbr5x5", 0)
        #print(observations)

        print("nbr5x5: ", nbr)
        
        self.whatMyPos(observations)
        print("\n>>> nb4tf4i arena --- (there are observations) -------------------")
        print("Steve's Coords: ", self.x, self.y, self.z, " Yaw: ", self.yaw, " Pitch: ", self.pitch)        

        #flower, dirt, dirt_idx = self.checkInventory(observations)
        #print("Number of flowers: ", self.nof_red_flower)

        #self.whatISee(observations)
        #print("    Steve's <): ", self.lookingat)
                        
        self.calcNbrIndex()

        #print("5x5 grid", nbr)

        delay = .2

        if self.state == SteveState.PREPARE :
        	if self.y <= 25 :
        		self.agent_host.sendCommand( "move 1" );
        		self.agent_host.sendCommand( "jumpmove 1" );
        		delay = .01
        	else :
        		self.agent_host.sendCommand( "move 1" );
        		self.agent_host.sendCommand( "move 1" );
        		self.agent_host.sendCommand( "turn 1" );
        		self.state = SteveState.SPIRAL


        elif self.state == SteveState.SPIRAL :
        	if nbr[12+25+25] == 'red_flower' or nbr[self.front_of_me_idx+25] == 'red_flower' :
        		print("FLOWERRR***********")
        		self.state = SteveState.RIGH_FLOWER
        	
        	elif nbr[self.front_of_me_idx+25] == 'dirt' :
        		self.agent_host.sendCommand( "turn 1" );
        	else :
        		self.agent_host.sendCommand( "move 1" )
        		delay = .05

        elif self.state == SteveState.RIGH_FLOWER :
        	print("kiszedes!!!!!!!!!!!!!4")
        	self.agent_host.sendCommand( "move -1" );
        	self.agent_host.sendCommand( "look 1" );
        	self.agent_host.sendCommand( "look 1" );
        	time.sleep(.2)
        	self.agent_host.sendCommand( "attack 1" );
        	time.sleep(.5)
        	self.agent_host.sendCommand( "jumpmove 1" );
        	time.sleep(.15)
        	self.agent_host.sendCommand( "attack 1" );
        	time.sleep(.5)
        	self.agent_host.sendCommand( "turn 1" );
        	#time.sleep(1)
        	self.agent_host.sendCommand( "look -1" );
        	self.agent_host.sendCommand( "look -1" );
        	time.sleep(.1)
        	self.agent_host.sendCommand( "attack 1" );
        	time.sleep(.05)
        	self.agent_host.sendCommand( "jumpmove 1" );
        	time.sleep(.2)
        	self.agent_host.sendCommand( "move 1" );
        	self.agent_host.sendCommand( "turn -1" );
        	time.sleep(.2)
        	self.agent_host.sendCommand( "move -1" );
        	self.agent_host.sendCommand( "move -1" );
        	#delay = 2

        	self.state = SteveState.SPIRAL


        return delay

num_repeats = 1
for ii in range(num_repeats):

    my_mission_record = MalmoPython.MissionRecordSpec()

    # Attempt to start a mission:
    max_retries = 6
    for retry in range(max_retries):
        try:
            agent_host.startMission( my_mission, my_mission_record )
            break
        except RuntimeError as e:
            if retry == max_retries - 1:
                print("Error starting mission:", e)
                exit(1)
            else:
                print("Attempting to start the mission:")
                time.sleep(2)

    # Loop until mission starts:
    print("   Waiting for the mission to start ")
    world_state = agent_host.getWorldState()

    while not world_state.has_mission_begun:
        print("\r"+hg.cursor(), end="")
        time.sleep(0.15)
        world_state = agent_host.getWorldState()
        for error in world_state.errors:
            print("Error:",error.text)

    print("NB4tf4i Red Flower Hell running\n")
    steve = Steve(agent_host)
    steve.run()

print("Mission ended")
# Mission has ended.



