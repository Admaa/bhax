class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host
        
        self.nof_red_flower = 0

    def run(self):
        world_state = self.agent_host.getWorldState()
        t = 0
        level = 0
        hossz = 8
        # Loop until mission ends:
        self.agent_host.sendCommand( "look 1" )
        while world_state.is_mission_running:
            print("--- nb4tf4i arena -----------------------------------\n")
            if world_state.number_of_observations_since_last_state != 0:
                
                sensations = world_state.observations[-1].text
                print("    sensations: ", sensations)                
                observations = json.loads(sensations)
                nbr3x3x3 = observations.get("nbr3x3", 0)
                print("    3x3x3 neighborhood of Steve: ", nbr3x3x3)
                
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])        
                if "YPos" in observations:
                    self.y = int(observations["YPos"])  
                
                print("    Steve's Coords: ", self.x, self.y, self.z)        
                print("    Steve's Yaw: ", self.yaw)        
                print("    Steve's Pitch: ", self.pitch)

                if "LineOfSight" in observations:
                    lineOfSight = observations["LineOfSight"]
                    self.lookingat = lineOfSight["type"]
                print(" Steve's <): ", self.lookingat)

                if self.lookingat =="red_flower":
                    print("P I P A C S")
                    time.sleep(.5)
                    self.agent_host.sendCommand( "attack 1" )
                    time.sleep(.5)


            self.agent_host.sendCommand( "move 1" )
            time.sleep(.1)

            t = t + 1

            if t == hossz:
                t = 0
                level = level + 1
                if level == 5:
                    self.agent_host.sendCommand( "jumpmove 1" )
                    time.sleep(0.2)
                    self.agent_host.sendCommand( "move 1" )
                    time.sleep(0.2)
                    level = 0
                    hossz = hossz + 4
                    print("mostugrik")
                
                self.agent_host.sendCommand( "turn 1" )
                time.sleep(.5)
            print(level)   
 
            world_state = self.agent_host.getWorldState()
