class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host
        
        self.nof_red_flower = 0

    def run(self):
        world_state = self.agent_host.getWorldState()
        # Loop until mission ends:
        seta = 3;
        self.agent_host.sendCommand( "turn 1" )
        time.sleep(1)
        self.agent_host.sendCommand( "turn 0" )
        self.agent_host.sendCommand( "jump 1" )
        self.agent_host.sendCommand( "move 1" )
        time.sleep(1)
        self.agent_host.sendCommand( "move 0" )
        self.agent_host.sendCommand( "turn -1" )
        time.sleep(0.47)
        self.agent_host.sendCommand( "turn 0" )
        self.agent_host.sendCommand( "jump 0" )


        while world_state.is_mission_running:
            print("--- nb4tf4i arena -----------------------------------\n")
            self.agent_host.sendCommand( "move 1" )
            time.sleep(seta)
            self.agent_host.sendCommand( "jump 1")
            time.sleep(0.3)
            self.agent_host.sendCommand( "jump 0")
            self.agent_host.sendCommand( "move 0" )
            self.agent_host.sendCommand( "turn -1" )
            time.sleep(0.47)
            self.agent_host.sendCommand( "turn 0")
            seta += 1.8
            world_state = self.agent_host.getWorldState()
