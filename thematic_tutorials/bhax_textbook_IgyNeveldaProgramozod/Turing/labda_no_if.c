#include <stdlib.h>
#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main ( void )
{

    WINDOW *ablak;
    ablak = initscr ();

    int x1 = 0;
    int x2 = 0;
    int y1 = 0;
    int y2 = 0;

    int x_speed = 5;
    int y_speed = 2;


    int mx, my;

    for ( ;; ) {

        getmaxyx(ablak, my, mx);

        mx *= 2;
        my *= 2;

        x1 = (x1 - x_speed) % mx;
        x2 = (x2 + x_speed) % mx;
        y1 = (y1 - y_speed) % my;
        y2 = (y2 + y_speed) % my;

        clear();

        mvprintw(abs((y1 + (my - y2)) / 2), abs((x1 + (mx - x2)) / 2), "o");

        refresh();
        usleep(100000);

    }

    return 0;
}
