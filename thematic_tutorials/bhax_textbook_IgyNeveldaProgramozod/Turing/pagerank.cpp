#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

void kiir (vector<double> &vektor, int db){
	for (int i = 0; i < db; i++){
		cout << "Az " << i+1 << ". oldal pagerankja: " << vektor[i] << endl; 
	}
}

double tavolsag( vector<double> &PR, vector<double> &PRv, int n){
	double osszeg = 0.0;
	for (int i = 0; i < n; i++){
		osszeg += (PRv[i] - PR[i]) * (PRv[i] - PR[i]);
	}
	
	return sqrt (osszeg);
}


int main (void){
	vector<vector<double> >L = {
	{0.0, 	0.0, 	 	1.0 / 3.0, 	0.0},
	{1.0, 	1.0 / 2.0, 	1.0 / 3.0, 	1.0},
	{0.0, 	1.0 / 2.0, 	0.0, 		0.0},
	{0.0, 	0.0, 	 	1.0 / 3.0, 	0.0}
	};

	vector<double> PR = {0.0, 0.0, 0.0, 0.0};
	vector<double> PRv  = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0}; 

	int i, j;

	for(;;){
		for (i = 0; i < PR.size(); i++){	//4 helyett PR.size()
			PR[i] = 0.0;
			for (j = 0; j < PR.size(); j++){
				PR[i] += (L[i][j] * PRv[j]);
			}
		}

		if (tavolsag(PR, PRv, 4) < 0.0000000001){
			break;
		}

		for (int i = 0; i < 4; i++){
			PRv[i] = PR[i];
		}
	}

	kiir(PR, 4);

}